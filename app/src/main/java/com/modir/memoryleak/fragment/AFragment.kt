package com.modir.memoryleak.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.modir.memoryleak.R
import com.modir.memoryleak.databinding.FragmentABinding
import kotlinx.android.synthetic.main.fragment_a.*

class AFragment : Fragment() {

    private var _binding: FragmentABinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentABinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnFragmentA.setOnClickListener {
            val fragment = BFragment()
            activity?.supportFragmentManager?.beginTransaction()?.replace(
                R.id.container,
                fragment
            )
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null

    }

}