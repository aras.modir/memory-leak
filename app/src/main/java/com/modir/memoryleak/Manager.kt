package com.modir.memoryleak

import android.content.Context

class Manager private constructor(private val context: Context) {
    companion object {
        private var ourInstance: Manager? = null
        fun getInstance(context: Context): Manager? {
            if (ourInstance == null) {
                ourInstance = Manager(context)
            }
            return ourInstance
        }
    }

}