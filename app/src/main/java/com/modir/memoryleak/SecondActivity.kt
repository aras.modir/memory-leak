package com.modir.memoryleak

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.modir.memoryleak.fragment.AFragment

class SecondActivity : AppCompatActivity() {
    private var fragment = Fragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            fragment = AFragment()
            supportFragmentManager.beginTransaction().add(
                R.id.container,
                fragment
            )
                .commit()
        }
    }
}